<?php


interface Production
{
    public function getProduct();
    public function getProductType();
}