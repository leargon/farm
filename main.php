<?php
require_once 'Production.php';
require_once 'Animals.php';
require_once 'Stable.php';
require_once 'Cow.php';
require_once 'Chicken.php';

$farm = new Stable();

// Добавляем коров
for ($i = 0; $i < 10; $i++)
{
    $cow = new Cow('Cow '.$i, 'milk');
    $farm->addAnimal($cow);
}

// Добавляем кур
for ($i = 0; $i < 20; $i++)
{
    $chicken = new Chicken('Chicken '.$i, 'egg');
    $farm->addAnimal($chicken);
}

// Производим сбор молока и яиц
$farm->gather();

echo 'Собрано молока: ' . $farm->getTotalProducts()['milk'] . " л\r\n ";
echo 'Собрано яиц: ' . $farm->getTotalProducts()['egg'] . ' шт';



