<?php


class Stable
{
    private $animals = [];
    private $totalProducts = [];

    public function addAnimal(Animals $animal): void
    {
        $this->animals[] = $animal;
    }

    public function gather()
    {

        foreach ($this->animals as $animal)
        {
            @$this->totalProducts[$animal->getProductType()] += $animal->getProduct();
        }
    }

    public function getAnimals():array
    {
        return $this->animals;
    }

    public function getTotalProducts()
    {
        return $this->totalProducts;
    }
}