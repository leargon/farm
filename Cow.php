<?php


class Cow extends Animals
{

    public function __construct($name, $productType)
    {
        $this->productType = $productType;
        $this->name = $name;
        $this->productPerDay = rand(8,12);
    }

}