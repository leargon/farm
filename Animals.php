<?php


abstract class Animals implements Production
{
    protected $productPerDay;
    protected $name;
    protected $productType;

    public function getProduct():int
    {
        return $this->productPerDay;
    }

    public function getProductType()
    {
        return $this->productType;
    }
}