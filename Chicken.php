<?php


class Chicken extends Animals
{
    public function __construct($name, $productType)
    {
        $this->productType = $productType;
        $this->name = $name;
        $this->productPerDay = rand(0,1);
    }
}